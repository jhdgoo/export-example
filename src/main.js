import {createApp} from 'vue'
import {createRouter, createWebHistory} from 'vue-router';
import App from './App.vue'

const lazyLoad = v => { return () => import(`@/components/${v}`) }
const router = new createRouter({
    routes: [
        { path: '/', component: lazyLoad('HelloWorld') }
    ],
    history: createWebHistory()
});

createApp(App).use(router).mount('#app');
